from base45 import b45decode
import zlib
import cbor
from datetime import datetime

# scan your QR Code using e.g. zbar-tools (zbarimg)

qr_decoded = b"HC1:DATA_OF_QR_CODE"
step1 = b45decode(qr_decoded[4:])
step2 = zlib.decompress(step1)
step3 = cbor.loads(step2)
step4 = step3.value
step5 = step4[2]
step6 = cbor.loads(step5)

ISSUER_CLAIM = 1
EXPIRATION_CLAIM = 4
IAT_CLAIM = 6

HCERT_CLAIM = -260
EU_DGC_V1_CLAIM = 1

print("Country:", step6[ISSUER_CLAIM])
print("Issue date (UTC):", datetime.utcfromtimestamp(step6[IAT_CLAIM]).strftime('%Y-%m-%d %H:%M:%S'))
print("Expiration date (UTC):", datetime.utcfromtimestamp(step6[EXPIRATION_CLAIM]).strftime('%Y-%m-%d %H:%M:%S'))

step7 = step6[HCERT_CLAIM][EU_DGC_V1_CLAIM]

print("Name:", step7["nam"])
print("Date of birth:", step7["dob"])
print("Schema version:", step7["ver"])
print("Certificate info: ...")
for step8 in step7["v"]:
	print("Unique Certificate Identifier:", step8["ci"])
	print("Country:", step8["co"])
	print("Date of vaccination:", step8["dt"])
	print("Dose number:", step8["dn"])
	print("Doses required:", step8["sd"])
	print("Targeted disease:", step8["tg"], "(expected value: COVID-19 = 840539006)")
	print("Vaccine type:", step8["vp"], "(e.g. mRNA = 1119349007)")
	print("Vaccine manufacturer:", step8["ma"], "(e.g. Biontech = ORG-100032015)")
	print("Medical product ID:", step8["mp"], "(e.g. Comirnaty = EU/1/20/1528)")
	print("Certificate issuer:", step8["is"])